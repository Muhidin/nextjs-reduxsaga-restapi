import { all, call, put, takeLatest } from "redux-saga/effects";
import * as t from "../types";

import { apiCall } from '../services/productService';
import { ApiUrl } from '../services/apiUrl';

function* fetchProduct() {
    
    try {
        const response = yield call(apiCall, "GET", ApiUrl+"products") //axios(config);

        //console.log(response);

        const productList = yield response;

        yield put({
            type: t.PRODUCT_FETCH_SUCCEEDED,
            payload: productList.data,
        });
        
    } catch (error) {
        yield put({
            type: t.PRODUCT_FETCH_FAILED,
            payload: error.message,
        });
    }
}

function* watchFetchProduct() {
    yield takeLatest(t.PRODUCT_FETCH_REQUESTED, fetchProduct);
}

function* addProduct(action) {
    try {
        const datanya = JSON.stringify(action.payload);
        const response = yield call(apiCall, "POST", ApiUrl+"products", datanya)
        const newProduct = yield response;
        yield put({
			type: t.PRODUCT_ADD_SUCCEEDED,
			payload: newProduct.data,
		});
    } catch (error) {
        yield put({
			type: t.PRODUCT_ADD_FAILED,
			payload: error.message,
		});
    }
}

function* watchAddProduct() {
    yield takeLatest(t.PRODUCT_ADD_REQUESTED, addProduct);
}

function* deleteProduct(action) {
    try {
        const response = yield call(apiCall, "DELETE", ApiUrl+"products/"+action.payload)
        const deleteProduct = yield response;

        yield put({
            type: t.PRODUCT_DELETE_SUCCEEDED,
            payload: deleteProduct.data,
        })
    } catch (error) {
        yield put({
			type: t.PRODUCT_DELETE_FAILED,
			payload: error.message,
		});
    }
}

function* wactchRemoveProduct() {
    yield takeLatest(t.PRODUCT_DELETE_REQUESTED, deleteProduct);
}

function* updateProduct(action) {
    let datanya = JSON.stringify(action.payload);
    const response = yield call(apiCall, "POST", ApiUrl+"products/"+action.payload._id, datanya)

    const updateProduct = yield response;
    try {
        
        yield put({
            type: t.PRODUCT_UPDATE_SUCCEEDED,
            payload: updateProduct.data,
        })
    } catch (error) {
        yield put({
            type: t.PRODUCT_UPDATE_FAILED,
            payload: error.message,
        })
    }
}

function* watchUpdateProduct() {
    yield takeLatest(t.PRODUCT_UPDATE_REQUESTED, updateProduct);
}

export default function* rootSaga() {
    yield all([
        watchFetchProduct(),
        watchAddProduct(),
        watchUpdateProduct(),
        wactchRemoveProduct(),
    ])
}