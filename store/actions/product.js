import * as t from "../types";

export const setModalOpen = (isModalOpen) => {
    return {
        type: t.MODAL_OPEN,
        payload: isModalOpen,
    }
}

export const fetchProduct = () => {
    return {
        type: t.PRODUCT_FETCH_REQUESTED,
    };
}