import { HYDRATE } from "next-redux-wrapper";
import * as t from "../types";

const initialState = {
    productList: [],
    selectedProduct: undefined,
    isModalOpen: false,
}

const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case HYDRATE:
            return {...state, ...action.payload}
        case t.MODAL_OPEN:
            return {
                ...state,
                isModalOpen: action.payload,
            }
        case t.PRODUCT_FETCH_SUCCEEDED:
            return {
                ...state,
                productList: action.payload
            }
        case t.PRODUCT_ADD_SUCCEEDED:
            return {
                ...state,
                productList: [action.payload, ...state.productList],
            }
        case t.PRODUCT_UPDATE_SUCCEEDED:
            const updateProduct = state.productList.map((product) => {
                if (product._id === action.payload._id) {
                    return {
                        ...product,
                        product_id: action.payload.product_id,
                        product_name: action.payload.product_name,
                        product_price: action.payload.product_price,
                        product_unit: action.payload.product_unit,
                        product_stock: action.payload.product_stock,
                        product_category: action.payload.product_category,
                    };
                }

                return product;
            });

            return {...state, productList: updateProduct};

        default:
            return state;
    }
}

export default mainReducer;