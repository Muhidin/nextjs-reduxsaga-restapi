export const MODAL_OPEN = "MODAL_OPEN";

export const PRODUCT_SELECTED = "PRODUCT_SELECTED";

// untuk fetch product
export const PRODUCT_FETCH_REQUESTED = "PRODUCT_FETCH_REQUESTED";
export const PRODUCT_FETCH_SUCCEEDED = "PRODUCT_FETCH_SUCCEEDED";
export const PRODUCT_FETCH_FAILED = "PRODUCT_FETCH_FAILED";


// untuk Add product
export const PRODUCT_ADD_REQUESTED = "PRODUCT_ADD_REQUESTED";
export const PRODUCT_ADD_SUCCEEDED = "PRODUCT_ADD_SUCCEEDED";
export const PRODUCT_ADD_FAILED = "PRODUCT_ADD_FAILED";

// untuk UPDATE product
export const PRODUCT_UPDATE_REQUESTED = "PRODUCT_UPDATE_REQUESTED";
export const PRODUCT_UPDATE_SUCCEEDED = "PRODUCT_UPDATE_SUCCEEDED";
export const PRODUCT_UPDATE_FAILED = "PRODUCT_UPDATE_FAILED";

// untuk DELETE product
export const PRODUCT_DELETE_REQUESTED = "PRODUCT_DELETE_REQUESTED";
export const PRODUCT_DELETE_SUCCEEDED = "PRODUCT_DELETE_SUCCEEDED";
export const PRODUCT_DELETE_FAILED = "PRODUCT_DELETE_FAILED";