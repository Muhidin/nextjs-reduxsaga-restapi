import { useSelector, useDispatch } from "react-redux";
// import { PencilSVG, TrashSVG } from "../icons";

import {
    fetchProduct,
    setModalOpen
} from "../store"
import { useEffect } from "react";

export function ProductList() {
    const state = useSelector((state) => state.product)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchProduct());
    } ,[dispatch])

    return (
		<table className="table">
			<thead className="table__head">
				<tr>
					<th>Product ID</th>
					<th>Product Name</th>
                    <th>Product Price</th>
                    <th>Product Unit</th>
                    <th>Product Stock</th>
                    <th>Product Category</th>
                    <th>Action</th>
				</tr>
			</thead>

			<tbody className="table__body">
                {console.log(state.productList)}
				{state.productList.map(({ _id,product_id, product_name, product_price, product_unit, product_stock, product_category }) => (
					<tr key={_id}>
						<td>{product_id}</td>
						<td>{product_name}</td>
                        <td>{product_price}</td>
						<td>{product_unit}</td>
                        <td>{product_stock}</td>
						<td>{product_category}</td>
						<td>
							<button
								className="btn btn__compact btn__edit"
								// onClick={() => {
								// 	dispatch(setSelectedEmployee(_id));
								// 	dispatch(setModalOpen(true));
								// }}
							>
								{/* <PencilSVG /> */}
                                edit
							</button>
							<button
								className="btn btn__compact btn__delete"
								// onClick={() => {
								// 	dispatch(deleteEmployee(_id));
								// }}
							>
								{/* <TrashSVG /> */}
                                hapus
							</button>
						</td>
					</tr>
				))}
			</tbody>
		</table>
	);
}
